#!/bin/bash

INSTS_PER_CAT=10

for ss in 3 4 5 6 10; do
    for tt in 5 8 10; do
        for fp in 0.4 0.6 0.8; do
            # maximum time for lt is 10 times the number of containers (minimum of 20)
            mt=$(expr ${ss}*${tt}*${fp}*10 | bc)
            mt=$(echo $mt | xargs printf "%.*f\n" 0)
            mt=$((${mt}<20?20:${mt}))
            for ms in 4 8; do
                python3 gen.py --seed ${RANDOM} --stacks ${ss} --tiers ${tt} --fillpct ${fp} --maxtime ${mt} --maxspread ${ms} --n ${INSTS_PER_CAT} --prefix gen_lt/genlt_s${ss}_t${tt}_fp${fp}_mt${mt}_ms${ms}
            done
        done
    done
done

