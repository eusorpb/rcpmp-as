#!/Library/Frameworks/Python.framework/Versions/3.4/bin/python3

import argparse
import random
import numpy as np
import sys
import traceback

def random_tw(args):
    start = random.randint(0, args.maxtime - args.minspread)
    # This is known as the "uniform enough" distribution...
    spread = random.randint(args.minspread, args.maxspread)
    end = min(args.maxtime, start + spread)
    return [start, end]

def gen_inst(args, instid):
    """
    Generation procedure (for now):
        For each container, pick a random time point within the total time
        horizon. Then choose a random time window for the container around the time point.
    """
    slots = args.stacks * args.tiers
    containers = int(round(slots * args.fillpct))
    tw = np.array([random_tw(args) for cc in range(containers)])
    blocking = np.zeros(shape=(containers, containers), dtype=int)
    for cca in range(containers):
        for ccb in range(containers):
            # Will container a block container b? else: do they overlap?
            if not (tw[cca,1] <= tw[ccb,0]):
                blocking[cca][ccb] = 1
            elif tw[cca,0] < tw[ccb,1] and tw[ccb,0] < tw[cca,1]:
                blocking[cca][ccb] = 1
                blocking[ccb][cca] = 1
    # Position the containers in the stacks
    stacks = [[] for ss in range(args.stacks)]
    non_full = list(range(args.stacks))
    not_chosen = list(range(containers))
    while len(not_chosen) > 0:
        # select random stack for a random container
        use_stack = random.choice(non_full)
        use_container = random.choice(not_chosen)
        stacks[use_stack].append(use_container + 1) # containers are 1-indexed
        not_chosen.remove(use_container)
        if len(stacks[use_stack]) == args.tiers:
            non_full.remove(use_stack)
    # Pad out the stacks with 0s
    for ii, ss in enumerate(stacks):
        if len(ss) < args.tiers:
            stacks[ii].extend([0] * (args.tiers - len(ss)))

    # Output the instance
    try:
        if args.prefix:
            fp = open("{0}_{1}.in".format(args.prefix, instid), 'w')
        else:
            fp = sys.stdout
        print("Stacks: {0}".format(args.stacks), file=fp)
        print("Tiers: {0}".format(args.tiers), file=fp)
        print("Containers: {0}".format(containers), file=fp)
        print("Blocking_matrix:", file=fp)
        for cca in range(containers):
            print(" ".join([str(vv) for vv in blocking[cca]]), file=fp)
        print("Locations:", file=fp)
        for ii, ss in enumerate(stacks):
            print("Stack {0}: ".format(ii), end="", file=fp)
            print(" ".join([str(vv) for vv in ss]), file=fp)
        print("\n\n\n(extra) Time windows:", file=fp)
        for ii, ttww in enumerate(tw):
            print("Container {1}: {0}".format(ttww, ii+1), file=fp)
    except:
        print(sys.exc_info())
        print("Error writing instance to file or stdout")
    finally:
        if args.prefix:
            fp.close()

def print_args(args):
    print("Generating robust pre-marshalling instance with arguments:")
    print("Stacks: {0}".format(args.stacks))
    print("Tiers: {0}".format(args.tiers))
    print("Fill %: {0}".format(args.fillpct))
    print("Max time: {0}".format(args.maxtime))
    print("Max spread: {0}".format(args.maxspread))
    print("Prefix: {0}".format(args.prefix))
    print("Num insts: {0}".format(args.n))
    print("Seed: {0}".format(args.seed))

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--stacks', '-s', type=int, default=3, help='Number of stacks.')
    parser.add_argument('--tiers', '-t', type=int, default=5, help='Number of tiers.')
    parser.add_argument('--fillpct', '-f', type=float, default=0.6, help='Percentage of slots to fill with containers.')
    parser.add_argument('--maxtime', '-m', type=int, default=20, help='Maximum time horizon (inclusive, time starts at 0).')
    parser.add_argument('--minspread', '-o', type=int, default=1, help='Minimum time spread of a container (inclusive).')
    parser.add_argument('--maxspread', '-p', type=int, default=4, help='Maximum time spread of a container (inclusive).')
    parser.add_argument('--n', '-n', type=int, default=1, help='Number of instances to output.')
    parser.add_argument('--prefix', '-r', type=str, help='Output prefix if instances should be output to files.')
    parser.add_argument('--seed', type=int, default=None, help='Random seed initialization.')
    args = parser.parse_args()

    if args.seed:
        random.seed(args.seed)
    print_args(args)
    for ii in range(args.n):
        gen_inst(args, ii)



