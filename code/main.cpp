#include <cstdlib>
#include <iostream>

#include <gflags/gflags.h>

#include "Instance.h"
#include "ResourceManager.h"
#include "IdaSearch.h"
#include "GroupFinder.h"

DEFINE_int32(cpuTime, -1, "CPU time limit (seconds). -1 for no limit (default)");
DEFINE_int64(memLimit, 5368709120, "Maximum memory limit (bytes). -1 for no limit (default=5GB)");
DEFINE_int32(relaxOptimize, 1, "Use an objective function in the relaxation or not? When no objective is use, all solutions are analyzed and the best lower bound is kept. {0 = no objective function, 1 = objective function} ");

int main(int argc, char** argv) {

    google::SetUsageMessage("./robustpm [flags] <input file>");
    google::ParseCommandLineFlags(&argc, &argv, true);

    if(argc != 2) {
        std::cerr << "Usage: ./robustpm [flags] <input file>" << std::endl;
//         std::quick_exit(1);
        exit(1);
    }
    ResourceManager& rm = ResourceManager::getResourceManager();
    if(FLAGS_cpuTime > 0) {
        std::cout << "[INFO] Setting CPU time limit to " << FLAGS_cpuTime << " seconds." << std::endl;
        rm.setCpuLimit(FLAGS_cpuTime, FLAGS_cpuTime + 10);
    } else {
        std::cout << "[INFO] No CPU limit set." << std::endl;
    }
    if(FLAGS_memLimit > 0) {
        std::cout << "[INFO] Setting memory limit to " << FLAGS_memLimit << " bytes (" << (FLAGS_memLimit / 1024.0 / 1024.0 / 1024.0) << " GB)." << std::endl;
        rm.setMemoryLimit(FLAGS_memLimit);
    } else {
        std::cout << "[INFO] No memory limit set." << std::endl;
    }

    double cpuTimeStart = ResourceManager::userTime();
    
    Instance inst(argv[1]);
    std::vector<int> groupasgn;
    find_groups(inst, groupasgn, FLAGS_relaxOptimize == 0);
    if(groupasgn.size() != inst.containers()) {
        std::cout << "[ERROR] Invalid total order group assignments returned." << std::endl;
        return -1;
    }
    for(short cc = 0; cc < inst.containers(); ++cc) {
        std::cout << "[INFO] Container " << cc << " assigned to group " << groupasgn[cc] << std::endl;
    }
    inst.assignGroups(groupasgn);
    inst.print_all();
    std::cout << std::endl;
    std::cout << "[INFO] Starting in configuration: " << std::endl;
    std::cout << "===========" << std::endl;
    inst.print();
    std::cout << "===========" << std::endl;
    std::cout << "[INFO] Tiers: " << inst.tiers() << "; stacks: " << inst.stacks() << std::endl;

#ifdef EXTRAINFO
    std::cout << std::endl;
    std::cout << "[WARNING] The extra information flag is enabled! Compile without -DEXTRAINFO when running experiments!" << std::endl;
    std::cout << std::endl;
#endif

    IdaSearch ida(inst);
    short optMoves = ida.idaStarSearch();
    
    if(optMoves >= 0) {
        ida.printSolutionTrail();
        std::cout << "[SOL] Optimal number of moves: " << optMoves << std::endl;
        std::cout << "[SOL-INFO] Nodes opened: " << ida.nodesOpened() << std::endl;
    } else {
        std::cout << "No solution found in given time allotment." << std::endl;
    }
    double totalTime = ResourceManager::userTime() - cpuTimeStart;
    std::cout << "[CPU] Total CPU time: " << totalTime << std::endl;
    std::cout << "[INFO] Processed " << std::fixed << (ida.nodesOpened() / totalTime) << " nodes/second" << std::endl;

#ifdef EXTRAINFO
    std::cout << "[INFO] Unrelated stack symmetries broken: " << ida.sbUnrelCount() << std::endl;
    std::cout << "[INFO] Unrelated stack (multi-level) symmetries broken: " << ida.sbUnrelMultiCount() << std::endl;
    std::cout << "[INFO] Single-level transitive symmetries broken: " << ida.sbTransCount() << std::endl;
    std::cout << "[INFO] Multi-level transitive symmetries broken: " << ida.sbTransMLCount() << std::endl;
    std::cout << "[INFO] Empty symmetries broken: " << ida.sbEmptyCount() << std::endl;
#endif

    return 0;
}


