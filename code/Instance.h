#ifndef PM_INSTANCE
#define PM_INSTANCE

#include <fstream>
#include <vector>

#include "typedefs.h"

class Instance {
public:
    Instance(const char* instPath);

    inline ushort tiers() const { return m_tiers; }
    inline ushort stacks() const { return m_stacks; }
    inline ushort containers() const { return m_containers; }
    inline cell latestPriority() const { return m_latestprio; } 
    inline const std::vector<cell>& cells() const { return m_cells; }
    inline const std::vector<cell>& ccells() const { return m_ccells; }
    // Returns whether or not cell top blocks cell bot
    inline const bool blocking(cell top, cell bot) const { return m_blocking[top][bot]; }

    void assignGroups(const std::vector<int>& groupasgn);

    // Prints full instance information, including the blocking matrix
    void print_all() const;
    // Prints out where each container is currently stored (showing group values)
    void print() const;
    // Prints out where each container is currently stored (showing IDs)
    void printc() const;

private:
    inline cell& atCell(const int& tier, const int& stack) {
        return m_cells[tier * m_stacks + stack];
    }
    
    inline const cell& atCell(const int& tier, const int& stack) const {
        return m_cells[tier * m_stacks + stack];
    }

    inline cell& atCCell(const int& tier, const int& stack) {
        return m_ccells[tier * m_stacks + stack];
    }
    
    inline const cell& atCCell(const int& tier, const int& stack) const {
        return m_ccells[tier * m_stacks + stack];
    }

private:
    short m_tiers;
    short m_stacks;
    short m_containers;
    short m_latestprio;
    std::vector<std::vector<bool> > m_blocking;
    // 1-dimensional array stored with format: [tier][stack]; The bottom of each stack is located at tier 0.
    std::vector<cell> m_cells; 
    // ccells stores the container number in each cell, whereas m_cells stores the relaxation of the problem into group values 
    std::vector<cell> m_ccells; 
};


#endif

