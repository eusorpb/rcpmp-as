#include <iostream>
#include <sstream>
#include <fstream>
#include <cstdlib> // Really ought to use boost instead for the system command
#include <cstring> // For a temp filename

#include <gflags/gflags.h>

#include "Instance.h"

#include "GroupFinder.h"

#include "IdaSearch.h" // TODO tmp

DEFINE_string(minionPath, "minion", "Path to the minion executable (defaults to environment accessible).");
DEFINE_string(minizincPath, "mzn-g12lazy", "Path to the MiniZinc executable (mzn-g12fd, mzn-g12lazy, mzn-gecode)");
// DEFINE_int32(minionOrMinizinc, 0, "Use minion or MiniZinc? {0 = MiniZinc, 1 = Minion}");
DEFINE_int32(relaxNsols, 500, "Number of solutions to generate when the relaxOptimize option is disabled. Set to -1 for as many solutions as possible within the timeout.");
DEFINE_int32(relaxTimeout, 1, "Number of seconds to allow the relaxation to find a solution. If optimization of the groups is enabled and no solution is found within this timeout, the objective function is removed and the model is run again with this timeout.");
DEFINE_bool(extraLBInfo, false, "Prints out LB information about every relaxation LB found; used only for experimentation and debugging.");

void find_groups(const Instance& inst, std::vector<int>& sol, bool all) {
//     if(FLAGS_minionOrMinizinc == 0) {
        return find_groups_minizinc(inst, sol, all);
//     } else {
//         return find_groups_minion(inst, sol, all);
//     }
}

void find_groups_minizinc(const Instance& inst, std::vector<int>& sol, bool all) {
    sol.clear();
    char *tmpname = strdup("/tmp/tmpfileXXXXXX");
    mkstemp(tmpname);
    int bestlb = -1;
    std::stringstream ss;
    ss << tmpname;
    ss << ".mzn";
    std::string infile = ss.str();
    std::ofstream fp(infile.c_str());
    std::cout << "[INFO] Outputting minizinc file to " << infile << std::endl;
    if(fp.is_open()) {
        fp << "int: containers;" << std::endl;
        fp << "containers = " << inst.containers() << ";" << std::endl;
        fp << "set of int: CONTAINERS = 1..containers;\narray[CONTAINERS] of var CONTAINERS: gv;\nvar 1..(containers*containers): obj;" << std::endl;
        for(short cca = 0; cca < inst.containers(); ++cca) {
            for(short ccb = cca + 1; ccb < inst.containers(); ++ccb) {
                // We can't stack ccb on top of cca, but we can stack cca on ccb
                if(inst.blocking(ccb, cca) && !inst.blocking(cca, ccb)) {
                    fp << "constraint gv[" << (cca+1) << "] < gv[" << (ccb+1) << "];" << std::endl;
                } else if(!inst.blocking(ccb, cca) && inst.blocking(cca, ccb)) {
                    fp << "constraint gv[" << (ccb+1) << "] < gv[" << (cca+1) << "];" << std::endl;
                } else if(inst.blocking(ccb, cca) && inst.blocking(cca, ccb)) {
                    fp << "constraint gv["  << (cca+1) << "] != gv[" << (ccb+1) << "];" << std::endl;
                }
            }
        }
        fp << "constraint obj = sum(gv);" << std::endl;
        if(all) {
            fp << "solve satisfy;" << std::endl;
        } else {
            fp << "solve minimize obj;" << std::endl;
        }
        fp << "output [show(gv[ii]) ++ \" \" | ii in 1..containers];" << std::endl;
        fp.close();
    } else {
        std::cout << "[ERROR] Unable to open temp file " << tmpname << " for output." << std::endl;
        sol.clear();
        return;
    }
    // Execute minizinc on the model
    std::stringstream cmd;
    std::stringstream minionoutss;
    minionoutss << infile << "_minizincoutput";
    std::string minionout = minionoutss.str();
    cmd << "timeout " << FLAGS_relaxTimeout << " "; // There might be better ways of doing this than with ulimit...
    cmd << FLAGS_minizincPath << " ";
    if(all && FLAGS_relaxNsols > 0) {
        cmd << "-n " << FLAGS_relaxNsols << " ";
    } else if(all) {
        cmd << "-a ";
    }
    cmd << infile << " > " << minionout;
    system(cmd.str().c_str());
    std::vector<int> tmpsol;
    std::ifstream mfp(minionout.c_str());
    int solsexamined = 0;
    if(mfp.is_open()) {
        tmpsol.resize(inst.containers(), 0);
        std::string enddelim;
        bool hadone = false;
        bool isdone = false;
        do {
            int concount = 0;
            for(int cc = 0; cc < inst.containers(); ++cc) {
                if(!(mfp >> tmpsol[cc])) {
                    if(!hadone) {
                        if(!all) { // Optimization objective; try the satisfaction objective with a single solution
                            FLAGS_relaxNsols = 1;
                            std::cout << "[INFO] Failed to find a relaxation solution with the optimization objective; removing objective function and trying again." << std::endl;
                            find_groups_minizinc(inst, sol, true);
                            return; // if nothing is found, the error will be returned from inside the sat call
                        }
                        sol.clear();
                        std::cout << "[ERROR] Unable to read group information from file." << std::endl;
                        std::cout << "[ERROR] Is minizinc accessible from the command line? See: http://http://www.minizinc.org/" << std::endl;
                        std::cout << "[ERROR] Is the timeout command from the GNU coreutils package available?" << std::endl;
                        return;
                    } else {
                        isdone = true;
                    }
                } else {
                    concount++;
                }
            }
            // Sometimes ulimit cuts off minizinc during output; ignore incomplete solutions
            if(concount != inst.containers()) {
                break;
            }
            hadone = true;
            mfp >> enddelim;
            Instance& cinst = const_cast<Instance&>(inst);
            cinst.assignGroups(tmpsol);
            IdaSearch idas(cinst, true);
            int tmplb = idas.heuristicCostBf();
            if(FLAGS_extraLBInfo) std::cout << "LB: " << tmplb << std::endl;
            solsexamined++;
            if(tmplb > bestlb) {
                std::cout << "[INFO] Found new best LB (" << tmplb << "; prev: " << bestlb << "). Copying to sol." << std::endl;
                sol.resize(inst.containers(), 0);
                std::copy(tmpsol.begin(), tmpsol.end(), sol.begin());
                bestlb = tmplb;
            }
        } while(all && !isdone); 
        if(FLAGS_extraLBInfo) std::cout << "[INFO] Number of relaxation solutions examined: " << solsexamined << std::endl;
    } else {
        std::cout << "[ERROR] Unable to open minizinc output file " << minionout << std::endl;
        std::cout << "[ERROR] Is minizinc accessible from the command line? See: http://http://www.minizinc.org/" << std::endl;
        std::cout << "[ERROR] Is the timeout command from the GNU coreutils package available?" << std::endl;
    }
}

// deprecated
// void find_groups_minion(const Instance& inst, std::vector<int>& sol, bool all) {
//     sol.clear();
//     char *tmpname = strdup("/tmp/tmpfileXXXXXX");
//     mkstemp(tmpname);
//     std::ofstream fp(tmpname);
//     std::cout << "[INFO] Outputting minion file to " << tmpname << std::endl;
//     if(fp.is_open()) {
//         fp << "MINION 3\n**VARIABLES**" << std::endl;
//         fp << "DISCRETE gv[" << inst.containers() << "] {1.." << inst.containers() << "}" << std::endl;
//         fp << "BOUND obj {1.." << (inst.containers() * inst.containers()) << "}" << std::endl;
//         fp << "**CONSTRAINTS**" << std::endl;
//         for(short cca = 0; cca < inst.containers(); ++cca) {
//             for(short ccb = cca + 1; ccb < inst.containers(); ++ccb) {
//                 // We can't stack ccb on top of cca, but we can stack cca on ccb
//                 if(inst.blocking(ccb, cca) && !inst.blocking(cca, ccb)) {
//                     fp << "ineq(gv[" << cca << "], gv[" << ccb << "], -1)" << std::endl;
//                 } else if(!inst.blocking(ccb, cca) && inst.blocking(cca, ccb)) {
//                     fp << "ineq(gv[" << ccb << "], gv[" << cca << "], -1)" << std::endl;
//                 } else if(inst.blocking(ccb, cca) && inst.blocking(cca, ccb)) {
//                     fp << "diseq(gv[" << cca << "], gv[" << ccb << "])" << std::endl;
//                 }
//             }
//         }
//         fp << "sumleq(gv, obj)\nsumgeq(gv, obj)\n**SEARCH**\nMINIMISING obj\n**EOF**" << std::endl;
//         fp.close();
//     } else {
//         std::cout << "[ERROR] Unable to open temp file " << tmpname << " for output." << std::endl;
//     }
//     // Execute minion on the model
//     std::stringstream cmd;
//     std::stringstream minionoutss;
//     minionoutss << tmpname << "_minionoutput";
//     std::string minionout = minionoutss.str();
//     cmd << FLAGS_minionPath << " -printsolsonly " << tmpname << " > " << minionout;
//     int retcode = system(cmd.str().c_str());
//     if(retcode == 0) {
//         std::cout << "Minion retcode: " << retcode << std::endl;
//         std::ifstream mfp(minionout.c_str());
//         if(mfp.is_open()) {
//             sol.resize(inst.containers(), 0);
//             for(int cc = 0; cc < inst.containers(); ++cc) {
//                 mfp >> sol[cc];
//             }
//         } else {
//             std::cout << "[ERROR] Unable to open minion output file " << minionout << std::endl;
//         }
//     } else {
//         std::cout << "[ERROR] Unexpected return code from minion solver (" << retcode << ")" << std::endl;
//         std::cout << "[ERROR] Is minion accessible from the command line? See: http://constraintmodelling.org/minion/" << std::endl;
//     }
// }


