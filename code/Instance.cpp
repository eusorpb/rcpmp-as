#include <iostream>
#include <iomanip>
#include <sstream>
#include <fstream>

#include "Instance.h"

Instance::Instance(const char* instPath) 
        : m_tiers(0), m_stacks(0), m_containers(0), m_blocking(), m_cells() {
    std::cout << "[INFO] Loading instance: " << instPath << std::endl;
    // TODO Change to blocking matrix type problems
    std::ifstream input(instPath);
    if(input.is_open()) {
        std::string tmp;
        while(input >> tmp) {
            if(tmp == "Stacks:") {
                input >> m_stacks;
            } else if(tmp == "Tiers:") {
                input >> m_tiers;
            } else if(tmp == "Containers:") {
                input >> m_containers;
            } else if(tmp == "Blocking_matrix:") {
                int blocktmp = 0;
                m_blocking = std::vector<std::vector<bool> >(m_containers, std::vector<bool>(m_containers, false));
                for(short cca = 0; cca < m_containers; ++cca) {
                    for(short ccb = 0; ccb < m_containers; ++ccb) {
                        input >> blocktmp;
                        m_blocking[cca][ccb] = blocktmp == 1;
                    }
                }
            } else if(tmp == "Locations:") {
                m_cells = std::vector<cell>(m_stacks * m_tiers, 0);
                m_ccells = std::vector<cell>(m_stacks * m_tiers, 0);
                for(short ss = 0; ss < m_stacks; ++ss) {
                    input >> tmp;
                    input >> tmp;
                    for(short tt = 0; tt < m_tiers; ++tt) {
                        input >> atCCell(tt, ss);
                    }
                }
            }
        }
    input.close();  
    } else {
        std::cerr << "Instance not found: " << instPath << std::endl;
        exit(1);
    }
}

void Instance::assignGroups(const std::vector<int>& groupasgn) {
    short maxgroup = 0;
    for(short ss = 0; ss < m_stacks; ++ss) {
        for(short tt = 0; tt < m_tiers; ++tt) {
            if(atCCell(tt, ss) > 0) {
                atCell(tt, ss) = groupasgn[atCCell(tt, ss) - 1];
                maxgroup = std::max(maxgroup, atCell(tt, ss));
            }
        }
    }
    m_latestprio = maxgroup;
}

void Instance::print_all() const {
    std::cout << "Stacks: " << m_stacks << std::endl;
    std::cout << "Tiers: " << m_tiers << std::endl;
    std::cout << "Containers: " << m_containers << std::endl;
    std::cout << "Blocking matrix: " << std::endl;
    for(short cca = 0; cca < m_containers; ++cca) {
        for(short ccb = 0; ccb < m_containers; ++ccb) {
            std::cout << (m_blocking[cca][ccb] ? 1 : 0) << " ";
        }
        std::cout << std::endl;
    }
    std::cout << "Container locations: " << std::endl;
    printc();
}

void Instance::printc() const {
    for(short tt = m_tiers - 1; tt >= 0; --tt) {
        for(short ss = 0; ss < m_stacks; ++ss) {
            if(atCCell(tt,ss) > 0) {
                std::cout << std::setw(3) << atCCell(tt, ss);
            } else {
                std::cout << std::setw(3) << " ";
            }
        }
        std::cout << std::endl;
    }
}

void Instance::print() const {
    for(short tt = m_tiers - 1; tt >= 0; --tt) {
        for(short ss = 0; ss < m_stacks; ++ss) {
            if(atCell(tt,ss) > 0) {
                std::cout << std::setw(3) << atCell(tt, ss);
            } else {
                std::cout << std::setw(3) << " ";
            }
        }
        std::cout << std::endl;
    }
}

