#ifndef RPM_GROUP_FINDER
#define RPM_GROUP_FINDER

#include "typedefs.h"

class Instance;

void find_groups(const Instance& inst, std::vector<int>& sol, bool all = false);
void find_groups_minizinc(const Instance& inst, std::vector<int>& sol, bool all = false);
// all is ignored for minion
// void find_groups_minion(const Instance& inst, std::vector<int>& sol, bool all = false);

#endif


