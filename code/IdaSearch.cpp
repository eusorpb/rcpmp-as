#include <cassert>
#include <cmath>
#include <iostream>
#include <iomanip>

#include <algorithm>
#include <limits>

#include <gflags/gflags.h>

#include "IdaSearch.h"
#include "Instance.h"
#include "ResourceManager.h"

DEFINE_int32(printFreq, 100000, "Printing frequency during search.");
DEFINE_int32(symBreakingUnrel, 1, "Enable single-level unrelated move symmetry breaking. 0 = off, 1 = less than (default), 2 = greater than, 3 = multi-level less than, 4 = multi-level greater than"); 
DEFINE_int32(symBreakingTrans, 2, "Transitive symmetry breaking? 0 = off, 1 = single level, 2 = multi-level");
DEFINE_bool(symBreakingEmpty, true, "Empty stack symmetry breaking? 0 = off, 1 = on");
// DEFINE_bool(greedyCompletion, false, "Attempts to complete solutions meeting certain criteria using a greedy minimum difference algorithm. 0 = off, 1 = on");
// DEFINE_bool(indirectMoveCosts, false, "Attempts to more accurately determine indirect move costs in the lower bound heuristic. 0 = off, 1 = on");
// DEFINE_bool(moveCap, true, "Cap the number of moves and slowly increase it based on failure? 0 = off, 1 = on");
DEFINE_string(lowerBound, "bf", "Which lower bound should be used? Options: bf = Bortfeldt & Forster; us = our lower bound; max = the maximum of BF and us lower bounds; choose = decide which lower bound to choose based on the current cell configuration; direct = count the direct moves (not a strong LB).");
DEFINE_bool(usLbRestrict, true, "Use a heuristic to determine when to run our lower bound if lowerBound is set to max. This attempts to avoid executions of our lower bound where it is not likely to find a higher bound than BF.");
DEFINE_bool(lbAndExit, false, "Just print out the lower bound and exit.");


IdaSearch::IdaSearch(const Instance& inst, bool quiet) 
        : m_inst(inst), 
          m_cells(),
          m_ccells(),
          m_stackTop(inst.stacks(), -1),
          m_empty(inst.stacks(), false),
//           m_numEmpty(0),
          m_moves(),
          m_lbDemand(inst.latestPriority() + 1, 0),
          m_lbSupply(inst.latestPriority() + 1, 0),
          m_lbHighestWplaced(inst.stacks(), -1),
          m_lbStackHasOverstowage(inst.stacks(), false),
          m_directMoves(0),
          m_tmlAllowed(m_inst.stacks(), false),
          m_tiersM1(inst.tiers() - 1),
          m_nodesOpened(0),
          m_sbUnrelCount(0),
          m_sbUnrelMultiCount(0),
          m_sbTransCount(0),
          m_sbTransMLCount(0),
          m_sbEmptyCount(0),
          m_greedyCompletionsAttempted(0),
          m_greedyCompletionsSuccess(0),
          m_bfBetter(0),
          m_customBetter(0),
          m_customSkipped(0),
          m_lbTotal(0),
          m_clearCost(inst.stacks(), 0),
          m_useLb(LB_BF) {
//           m_containerAtPos(inst.stacks() * inst.tiers(), 0),
//           m_numMoves(inst.stacks() * inst.tiers(), 0) {
//           m_canRunGreedy(false),
//           m_stackOverstowage(0) {
//     // Initialize bay
    m_cells.resize(m_inst.cells().size(), 0);
    m_ccells.resize(m_inst.cells().size(), 0);
    std::copy(m_inst.cells().begin(), m_inst.cells().end(), m_cells.begin());
    std::copy(m_inst.ccells().begin(), m_inst.ccells().end(), m_ccells.begin());
    // Initialize stack tops
    for(short ss = 0; ss < m_inst.stacks(); ++ss) {
        bool gotTop = false;
        for(short tt = m_tiersM1; tt >= 0 && !gotTop; --tt) {
            if(atCell(tt, ss) > 0) {
                m_stackTop[ss] = tt;
                gotTop = true;
            }
        }
        m_empty[ss] = !gotTop;
//         if(!gotTop) m_numEmpty++;
    }
    if(!quiet) {
        std::cout << "[INFO] Unrelated move symmetry breaking: ";
        switch(FLAGS_symBreakingUnrel) {
            case 0:
                std::cout << "disabled";
                break;
            case 1:
                std::cout << "enabled; single level less than";
                break;
            case 2:
                std::cout << "enabled; single level greater than";
                break;
            case 3:
                std::cout << "enabled; multi level less than";
                break;
            case 4:
                std::cout << "enabled; multi level greater than";
                break;
            default:
                std::cout << "unsupported option; defaulting to enabled less than";
                FLAGS_symBreakingUnrel = 1;
                break;
        }
        std::cout << std::endl;
        std::cout << "[INFO] Transitive symmetry breaking: ";
        switch(FLAGS_symBreakingTrans) {
            case 0:
                std::cout << "disabled";
                break;
            case 1:
                std::cout << "single level";
                break;
            case 2:
                std::cout << "multi level";
                break;
            default:
                std::cout << "unsupported option; defaulting to multi-level.";
                FLAGS_symBreakingTrans = 2;
                break;
        }
        std::cout << std::endl;
    }

    if(!quiet) std::cout << "[INFO] Setting lower bound to";
    if(FLAGS_lowerBound == "direct") {
        m_useLb = LB_DIRECT;
        if(!quiet) std::cout << " the direct moves lower bound.";
    } else {
        if(!quiet) std::cout << " the BF lower bound.";
        if(FLAGS_lowerBound != "bf") {
            if(!quiet) std::cout << std::endl << "[WARNING] Invalid lower bound selected (" << FLAGS_lowerBound << "). Setting lower bound to BF";
        }
        m_useLb = LB_BF;
    }
    if(!quiet) std::cout << std::endl;

//     ushort curContainer = 1;
//     for(ushort ss = 0; ss < m_inst.stacks(); ++ss) {
//         for(ushort tt = 0; tt < m_inst.tiers(); ++tt) {
//             const ushort& pos = cellOffset(tt, ss);
//             if(m_cells[pos] > 0) {
//                 m_containerAtPos[pos] = curContainer;
// //                 containerAtPos[pos] = m_cells[pos];
// //                 std::cout << "ss: " << ss << "; tt: " << tt << "; pos " << pos << " assigned to: " << containerAtPos[pos] << std::endl;
//                 curContainer++;
//             }
//         }
//     }
}

IdaSearch::~IdaSearch() {
}

short IdaSearch::idaStarSearch() {
    ushort treeDepth = heuristicCost();
    if(FLAGS_lbAndExit) {
        std::cout << treeDepth << std::endl;
        exit(0);
    }
//     int ubound = treeDepth * 2;
    std::cout << "[SEARCH] Root node LB: " << treeDepth << std::endl;

    if(isSolution()) {
        return treeDepth;
    }

    m_moves.reserve(treeDepth * 2); // prevent unnecessary allocations in the move array

    bool solutionFound = false;
    while(!solutionFound && ResourceManager::cpuLimitNotReached()) {
        std::cout << "[SEARCH] Starting to examine tree level: " << treeDepth << std::endl;
        if(FLAGS_symBreakingTrans > 1) {
            m_tmlAllowed.resize(treeDepth * m_inst.stacks(), false);
        }
        
        solutionFound = recurse(0, treeDepth);
//         if(treeDepth > ubound) exit(0); // Not sure when to declare a problem infeasible right now

        treeDepth++;
    }
    treeDepth--;
    return (solutionFound) ? treeDepth : -1;
}

ushort IdaSearch::heuristicCost() {
    switch(m_useLb) {
        case LB_BF:
            return heuristicCostBf();
            break;
        case LB_DIRECT:
            return heuristicCostCustom(true);
            break;
        default:
            break;
    }
    return heuristicCostBf(); // should never be called
}

ushort IdaSearch::heuristicCostBf() {
//     m_canRunGreedy = false;
    int overstowage = 0;
    // Computing lower bound from Borgfeldt & Forster 
    // badly placed container = overstowing container 

    std::fill(m_lbDemand.begin(), m_lbDemand.end(), 0);
    std::fill(m_lbSupply.begin(), m_lbSupply.end(), 0);
    std::fill(m_lbHighestWplaced.begin(), m_lbHighestWplaced.end(), -1);
    std::fill(m_lbStackHasOverstowage.begin(), m_lbStackHasOverstowage.end(), false);

//     std::vector<ushort> highestPriorityPos; // TODO make member

    // Compute n'_{BX}
    int stackOverstowed = 0;
    int minBadlyPlaced = std::numeric_limits<int>::max();
    int supplyValue = 0;
    for(short ss = 0; ss < m_inst.stacks(); ++ss) {
        bool afterHighestWp = false;
        bool overstowed = false;
        stackOverstowed = 0;
        const cell& bottom = atCell(0, ss);
        if(bottom > 0) {
            m_lbHighestWplaced[ss] = bottom;
        }

        for(short tt = 1; tt < m_inst.tiers(); tt++) {
            const cell& current = atCell(tt, ss);
            const cell& below = atCell(tt - 1, ss);
            if(!overstowed && (below < current)) {
                supplyValue = below;
                m_lbHighestWplaced[ss] = below;
                overstowed = true;
                m_lbStackHasOverstowage[ss] = true;
                afterHighestWp = true;
            }
            if(!afterHighestWp && bottom > 0 && current == 0) {
                afterHighestWp = true;
                supplyValue = below;
            }
            if(afterHighestWp) {
                m_lbSupply[supplyValue]++;
            }
            if(overstowed) {
                if(current != 0) {
                    m_lbDemand[current]++;
                    stackOverstowed++;
                }
            } else if(current > 0) {
                m_lbHighestWplaced[ss] = current;
            }
            // Indirect move costs calculation: take into account highest
            // priority container if it is currently in a bad spot.
//             if(FLAGS_indirectMoveCosts && current == m_inst.latestPriority() && overstowed) { 
//                 highestPriorityPos.push_back(cellOffset(tt, ss));
//             }
        }
        overstowage += stackOverstowed;
        minBadlyPlaced = std::min(minBadlyPlaced, stackOverstowed);
    }
//     std::cout << "[LB-B] Obvious overstowage: " << overstowage << "; minBadlyPlaced: " << minBadlyPlaced << std::endl;
//     m_stackOverstowage = overstowage;
//     std::stringstream sstr;
    m_directMoves = overstowage;
    overstowage += minBadlyPlaced;
//     m_canRunGreedy = (minBadlyPlaced == 0);

    // Compute the maximum cumulative demand surplus g*
    // Note that D(g) is computed on the fly by accumulating the values in the demand vector
    int cmDemand = 0;
    int numEmptyStacks = 0;
    for(auto itr = m_empty.begin(); itr != m_empty.end(); ++itr) {
        numEmptyStacks += (*itr) ? 1 : 0;
    }
    int cmSupply = numEmptyStacks * m_inst.tiers();
    int maxCmDmdSup = 0;
    int gstar = 0;
    for(short gg = m_inst.latestPriority(); gg > 0; --gg) {
        cmDemand += m_lbDemand[gg];
        cmSupply += m_lbSupply[gg];
        int tmpCmDmdSup = cmDemand - cmSupply;
        //     if(debug) std::cout << "[LB] gg: " << gg << "; demand: " << demand[gg] << "; supply: " << supply[gg] << "; cmDemand: " << cmDemand << "; cmSupply: " << cmSupply << "; tcds: " << tmpCmDmdSup << std::endl;
        if(tmpCmDmdSup > maxCmDmdSup) {
            maxCmDmdSup = tmpCmDmdSup;
            gstar = gg;
        }
    }
//     std::cout << "[LB-B] gstar: " << gstar << std::endl;
    int nsgx = std::max(0, (int)std::ceil(maxCmDmdSup / (double)m_inst.tiers()));
    std::vector<int> ngstar; // n^{g^*}(s) for all stacks in which the highest well placed item belongs to a group g < gstar
    ngstar.reserve(m_inst.stacks());

    for(size_t ss = 0; ss < m_inst.stacks(); ++ss) {
        if(m_lbHighestWplaced[ss] < gstar) {
            ngstar.push_back(0);
            if(!m_empty[ss]) {
                // iterate up stack ss until the current tier is overstowing
                for(size_t tt = 0; tt < m_inst.tiers() && atCell(tt, ss) > 0 && (tt == 0 || atCell(tt, ss) <= atCell(tt - 1, ss)); tt++) {
                    if(atCell(tt,ss) < gstar) ngstar.back()++;
                }
            }
        }
    }

    // Compute n'_{GX}; 
    std::sort(ngstar.begin(), ngstar.end());
//     std::cout << "[LB-B] nsgx: " << nsgx << "; ngs sz: " << ngstar.size() << ";" << std::endl;
//     std::cout << "[LB-B] ngstar: ";
    assert((int)ngstar.size() >= nsgx);
    for(int ii = 0; ii < nsgx; ++ii) {
        overstowage += ngstar[ii];
//         std::cout << ngstar[ii] << " ";
//         m_canRunGreedy = m_canRunGreedy && ngstar[ii] == 0;
    }
//     std::cout << std::endl;

//     if(FLAGS_indirectMoveCosts && m_numEmpty == 1) { // For now, only use this when there is an empty stack until I can show that it doesn't over estimate for other cases 
//         int dbgInc = 0;
//         for(auto hitr = highestPriorityPos.begin(); hitr != highestPriorityPos.end(); ++hitr) {
//             // Inspect containers above this one and see if they can be placed anywhere without overstowing
//             for(ushort cc = *hitr + m_inst.stacks(); cc < m_cells.size(); cc += m_inst.stacks()) {
//                 const cell& cur = m_cells[cc];
// //                 ushort numGood = 0;
//                 if(cur > 0) {
//                     bool hasGood = false;
//                     for(ushort ss = 0; ss < m_inst.stacks(); ++ss) {
//                         if(cellOffset(m_stackTop[ss], ss) != cc && !m_empty[ss]) {
//                             for(ushort tt = m_stackTop[ss]; tt < m_inst.tiers() - 1 && !hasGood; ++tt) {
//                                 // Weak form of this lower bound: if there is
//                                 // even one good spot for the container, then
//                                 // we do not increment the lower bound
//                                 hasGood = atCell(tt, ss) >= cur;
//                             }
//                         }
//                     }
//                     if(!hasGood) {
//                         overstowage++;
//                         dbgInc++;
//                     }
//                 }
//             }
//         }
//     }

    return overstowage;
}

ushort IdaSearch::heuristicCostCustom(bool onlyDirect) {
    std::fill(m_lbHighestWplaced.begin(), m_lbHighestWplaced.end(), -1);

    // The maximum priority overstowing container position
    ushort maxOverstowingPos = 0;
    ushort maxOverstowingVal = 0;
    ushort maxOverstowingStack = 0;

    // TODO This is getting computed twice when the BF bound is running...
    ushort directMoves = 0;
    for(short ss = 0; ss < m_inst.stacks(); ++ss) {
        bool overstowing = false;
        const cell& bottom = atCell(0, ss);
        if(bottom > 0) {
            m_lbHighestWplaced[ss] = bottom;
        }
        bool hitTop = false;
        for(short tt = 1; tt < m_inst.tiers() && !hitTop; tt++) {
            const cell& current = atCell(tt, ss);
            if(current > 0) {
                const cell& below = atCell(tt - 1, ss);
                if(!overstowing && (below < current)) {
                    m_lbHighestWplaced[ss] = below;
                    overstowing = true;
                }
                if(overstowing) {
                    directMoves++;
                    if(current > maxOverstowingVal) {
                        maxOverstowingVal = current;
                        maxOverstowingPos = cellOffset(tt, ss);
                        maxOverstowingStack = ss;
                    }
                }
            } else {
                hitTop = true;
            }
        }
    }
    if(onlyDirect) return directMoves;
//     std::cout << "[LB-C] Direct moves: " << directMoves << std::endl;

    ushort indirectMoves = 0;
    // First try to free the overstowing container
    for(ushort cc = maxOverstowingPos + m_inst.stacks(); cc < m_cells.size() && m_cells[cc] > 0; cc += m_inst.stacks()) {
//         bool hasGoodSpot = false; // TODO This is a weak version of the lower bound; could be strenthened by considering the movement of the containers above this one
//         const cell& blocking = m_cells[cc];
        ushort tier = maxOverstowingPos / m_inst.stacks();
        ushort stack = maxOverstowingPos % m_inst.stacks();
        if(!hasGoodPosition(tier, stack)) indirectMoves++;
    }
    std::cout << "[LB-C] Indirect from freeing moc: " << indirectMoves << std::endl;
    // Second, find a stack that offers the lowest number of indirect moves to
    // place the max overstowing container into.
    ushort minIndirect = std::numeric_limits<ushort>::max();
    for(ushort ss = 0; ss < m_inst.stacks(); ++ss) {
        ushort curIndirect = 0;
        if(ss == maxOverstowingStack) {
            // Assume the containers in the current stack have been removed,
            // including the moc. Then clear the remaining containers out and
            // attempt to replace the moc.
            curIndirect = 1; // moc is a direct move + moving it back into the stack
            ushort maxTier = maxOverstowingPos / m_inst.stacks();
            bool overstowing = false;
            for(ushort tt = 0; tt < maxTier; ++tt) {
                const cell& under = atCell(tt, ss);
                overstowing = overstowing || (tt > 0 && under > atCell(tt - 1, ss));
                if(!hasGoodPosition(tt, ss)) {
                    curIndirect += overstowing ? 1 : 2;
                }
            }
            std::cout << "[LB-C] stack " << ss << "; curIndirect: " << curIndirect << " (same-stack replacement) " << std::endl;
        } else {
            // Locate the position in this stack that the container would be placed into
            ushort tt = 0;
            // can only place the container in a non-overstowing position above a well-placed container
            bool overstowing = false;
            for(; tt < m_inst.tiers() && tt < m_stackTop[ss] && atCell(tt, ss) >= maxOverstowingVal; ++tt) {
                const cell& blocking = atCell(tt, ss);
                overstowing = overstowing || (tt > 0 && blocking > atCell(tt - 1, ss));
                std::cout << "\t[LB-C] 1 tt: " << tt << "; ss: " << ss << "; overstowing: " << overstowing << std::endl;
            }
            for(; tt < m_inst.tiers() && atCell(tt, ss) > 0; ++tt) {
                const cell& blocking = atCell(tt, ss);
                overstowing = overstowing || (tt > 0 && blocking > atCell(tt - 1, ss));
                std::cout << "\t[LB-C] 2 tt: " << tt << "; ss: " << ss << "; overstowing: " << overstowing << "; val: " << blocking << "; hasGoodPos: " << hasGoodPosition(tt, ss) << std::endl;
                if(!hasGoodPosition(tt, ss)) {
                    curIndirect += overstowing ? 1 : 2;
                }
            }
            std::cout << "[LB-C] stack " << ss << "; curIndirect: " << curIndirect << std::endl;
        }
        minIndirect = std::min(minIndirect, curIndirect);
    }
    indirectMoves += minIndirect;
    
    std::cout << "[LB-C] direct: " << directMoves << "; indirect: " << indirectMoves << std::endl;
    return directMoves + indirectMoves;
}

bool IdaSearch::isSolution() const {
//     printCurrentCells(true);
    for(short ss = 0; ss < m_inst.stacks(); ++ss) {
        for(ushort ttj = 0; ttj < m_tiersM1 - 1; ++ttj) {
            const cell& curj = atCCell(ttj, ss);
            if(curj == 0) break;
            for(ushort tti = ttj + 1; tti < m_inst.tiers(); ++tti) {
                const cell& curi = atCCell(tti, ss);
                if(curi == 0) break;
                if(m_inst.blocking(curi - 1, curj - 1)) {
//                     std::cout << "NO: " << curi << " blocks " << curj << "[" << m_inst.blocking(curi - 1, curj - 1) << "]" << std::endl;
                    return false;
                }
            }
        }
    }
//     std::cout << "YES" << std::endl;
    return true;
}

void IdaSearch::printCurrentCells(bool stackBottom) const {
    int fieldWidth = 3;
    for(short tt = m_tiersM1; tt >= 0; --tt) {
        if(stackBottom) std::cout << std::setw(fieldWidth) << " ";
        for(short ss = 0; ss < m_inst.stacks(); ++ss) {
            if(atCCell(tt,ss) > 0) {
                std::cout << std::setw(fieldWidth) << atCCell(tt, ss);
            } else {
                std::cout << std::setw(fieldWidth) << " ";
            }
        }
        std::cout << std::endl;
    }
    if(stackBottom) {
        std::cout << std::setw(fieldWidth) << " ";
        for(size_t ss = 0; ss < m_inst.stacks(); ++ss) {
            std::cout << "---";
        }
        std::cout << std::endl;
        std::cout << std::setw(fieldWidth) << "S: ";
        for(short ss = 0; ss < m_inst.stacks(); ++ss) {
            std::cout << std::setw(fieldWidth) << ss;
        }
        std::cout << std::endl;
    }
}

void IdaSearch::printSolutionTrail() {
    std::cout << "[SOL-INFO] Printing solution trail, starting from solution: " << std::endl;
    int mm = m_moves.size();
    for(auto ritr = m_moves.rbegin(); ritr != m_moves.rend(); ++ritr) {
        printCurrentCells(true);
//         ushort hcbf = heuristicCostBf();
        heuristicCostBf();
        // TODO DEBUG uncomment next 3 lines
//         ushort hcc = heuristicCostCustom();
//         std::cout << " (BF: " << hcbf << " f = " << (hcbf + mm) << "; custom: " << hcc << " f = " << (hcc + mm) << ")" << std::endl;
//         std::cout << "=== Move " << mm <<  ": Stack " << ritr->from << " to " << ritr->to << " ===" << std::endl;

//         // Note reversed because list is being traversed backwards
//         const ushort& fromPos = cellOffset(m_stackTop[ritr->to], ritr->to);
//         const ushort& toPos = cellOffset(m_stackTop[ritr->from] + 1, ritr->from);
//         m_containerAtPos[toPos] = m_containerAtPos[fromPos];
//         m_containerAtPos[fromPos] = 0;
//         m_numMoves[m_containerAtPos[toPos]]++;

        performMove(ritr->to, ritr->from);
        mm--;
    }
    printCurrentCells(true);
//     auto maxitr = std::max_element(m_numMoves.begin(), m_numMoves.end());
//     std::cout << "[INFO] Maximum number of moves of any single container: " << *maxitr << " (at pos: " << std::distance(m_numMoves.begin(), maxitr) << ")" << std::endl;
}

bool IdaSearch::recurse(ushort moves, ushort maxDepth) {
    ushort fvalue = moves + heuristicCost();
    if(fvalue > maxDepth || !ResourceManager::cpuLimitNotReached()) return false;
    // Only bother checking for a solution if we are currently at the maximum
    // depth. If there was a solution with a depth less than maxDepth, we would
    // have found it already!
    if(fvalue == maxDepth && isSolution()) return true;

//     if(FLAGS_greedyCompletion && m_canRunGreedy && m_stackOverstowage <= maxDepth - moves) {
// #ifdef EXTRAINFO
//         m_greedyCompletionsAttempted++;
// #endif
// //         std::cout << "Running greedy. Current moves: " << moves << "; fvalue: " << fvalue << std::endl;
//         int greedyMoves = greedyComplete(maxDepth - moves, true);
//         if(greedyMoves >= 0) {
//             // We don't need to branch and the optimal solution was found. Run
//             // the greedy completion one more time without resetting the
//             // solution at the end.
// #ifdef EXTRAINFO
//             m_greedyCompletionsSuccess++;
// #endif
//             greedyComplete(maxDepth - moves, false);
//             return true;
//         } // else: can't greedyily complete, so begin branching.
//     }

    bool tmlValid = false;
    size_t tmlOffset = 0;
    for(ushort fromStack = 0; fromStack < m_inst.stacks(); ++fromStack) {
        // Symmetry breaking: Multi-level transitive precomputation
        if(FLAGS_symBreakingTrans > 1) {
            tmlOffset = m_inst.stacks() * moves;
            std::fill(m_tmlAllowed.begin() + tmlOffset, m_tmlAllowed.begin() + tmlOffset + m_inst.stacks(), false);
            // Loop until either there are no more actions to examine or the
            // current from stack (i) is modified by an action
            auto ritr = m_moves.rbegin();
            for(; ritr != m_moves.rend() && ritr->to != fromStack; ++ritr) {
                m_tmlAllowed[tmlOffset + ritr->from] = true;
                m_tmlAllowed[tmlOffset + ritr->to] = true;
            }
            // tmlAllowed is only valid if we haven't reached the end of the action list
            tmlValid = ritr != m_moves.rend();
        }

        for(ushort toStack = 0; toStack < m_inst.stacks(); ++toStack) {
            // Allowed to move if: from/to stacks different and from stack isn't empty and to stack is not full
            if(fromStack != toStack && !m_empty[fromStack] && m_stackTop[toStack] < m_tiersM1
                    && (m_moves.size() == 0 || (fromStack != m_moves.back().to) || (toStack != m_moves.back().from))) {
                bool noSymVio = true;

                // Symmetry breaking: Unrelated move symmetry
                if(FLAGS_symBreakingUnrel > 0 && FLAGS_symBreakingUnrel <= 2
                        && m_moves.size() > 0 && (fromStack != m_moves.back().from 
                            && fromStack != m_moves.back().to && toStack != m_moves.back().from 
                            && toStack != m_moves.back().to)) {
                    if(FLAGS_symBreakingUnrel == 1) {
                        noSymVio = m_moves.back().from < fromStack;
                    } else {
                        noSymVio = m_moves.back().from > fromStack;
                    }
#ifdef EXTRAINFO
                    if(!noSymVio) m_sbUnrelCount++;
#endif
                }
                if(FLAGS_symBreakingUnrel >= 3 && FLAGS_symBreakingUnrel <= 4) { // multi level unrel sym breaking
                    // Examine previous moves to see if they are unrelated to
                    // this move. If so, accept this move according to the
                    // multi-level unrel sym breaking rule
                    // Stop examining previous moves if either the from or to stack is modified
                    
                    auto ritr = m_moves.rbegin();
                    while(noSymVio && ritr != m_moves.rend() 
                            && ritr->to != fromStack
                            && ritr->to != toStack
                            && ritr->from != fromStack
                            && ritr->from != toStack) {
                        if(ritr->to != fromStack && ritr->to != toStack && ritr->from != fromStack && ritr->from != toStack) {
                            if(FLAGS_symBreakingUnrel == 3) {
#ifdef EXTRAINFO
                                if(noSymVio && (ritr->from >= fromStack)) m_sbUnrelMultiCount++;
#endif
                                noSymVio = noSymVio && (ritr->from < fromStack);
                            } else {
#ifdef EXTRAINFO
                                if(noSymVio && (ritr->from <= fromStack)) m_sbUnrelMultiCount++;
#endif
                                noSymVio = noSymVio && (ritr->from > fromStack);
                            }
                        }
                        ritr++;
                    }

                }

                // Symmetry breaking: Single level Transitive move symmetry
                noSymVio = noSymVio && (FLAGS_symBreakingTrans == 0 || m_moves.size() == 0 || fromStack != m_moves.back().to);
#ifdef EXTRAINFO
                if(FLAGS_symBreakingTrans > 0 && m_moves.size() > 0 && fromStack == m_moves.back().to) m_sbTransCount++;
#endif
                
                // Symmetry breaking: Multi-level Transitive move symmetry
                if(FLAGS_symBreakingTrans > 1 && tmlValid) {
#ifdef EXTRAINFO
                    if(noSymVio && !m_tmlAllowed[tmlOffset + toStack]) {
                        m_sbTransMLCount++;
                    }
#endif
                    noSymVio = noSymVio && m_tmlAllowed[tmlOffset + toStack];
                }

                // Symmetry breaking: Empty stack symmetry
                if(FLAGS_symBreakingEmpty && m_empty[toStack]) {
                    // We are putting a container in an empty stack; make sure
                    // there are no empty stacks with a lower id than this one
#ifdef EXTRAINFO
                    bool emptyVio = false;
#endif
                    for(size_t ss = 0; ss < toStack && noSymVio; ++ss) {
                        noSymVio = noSymVio && !m_empty[ss];
#ifdef EXTRAINFO
                        emptyVio = emptyVio || m_empty[ss];
#endif
                    }
#ifdef EXTRAINFO
                    if(emptyVio) m_sbEmptyCount++;
#endif
                }

                if(noSymVio) {
//                     if(performMove(fromStack, toStack, FLAGS_moveCap, false)) {
                    performMove(fromStack, toStack);
                    m_moves.push_back(Move(fromStack, toStack));
                    m_nodesOpened++;
                    if(m_nodesOpened % FLAGS_printFreq == 0) {
                        std::cout << "[SEARCH] (" << std::setprecision(5) << std::fixed << ResourceManager::userTime() << "s) Nodes: " << m_nodesOpened << "; Max depth: " << maxDepth << std::endl;
//                         printCurrentCells(false);
                    }
                    if(recurse(moves + 1, maxDepth)) return true;
//                     performMove(toStack, fromStack, FLAGS_moveCap, true); // reverse move
                    performMove(toStack, fromStack); // reverse move
                    m_moves.pop_back();
//                     } else {
//                         return false; // perform move decided not to do the move, so bounce back up the tree.
//                     }
                }
            }
        }
    }
    return false;
}

bool IdaSearch::hasGoodPosition(const ushort& tier, const ushort& stack, ushort level) {
    // TODO Check if we can move the container out of its current stack and place it back in
    //
    // Case 1: Check if this container can be placed above any well-placed
    // container. If not, we know immediately that we cannot put it anywhere
    // without an indirect move.
    // Case 2: A stack exists where this container could be placed without any
    // overstowage, and the stack has no overstowing containers. No indirect
    // moves are necessary.
    // Case 3: A stack exists where this container could be placed without any
    // overstowage, and the stack is overstowed. If the overstowing containers
    // have good positions according to this algorithm, no overstowage is
    // necessary. If even one of the containers requires any indirect moves,
    // the container we are moving also requires an indirect move.

    if(level > 1) return true; // don't recurse more than a single level down. In this case, return that a good spot exists.

    const cell& cval = atCell(tier, stack);

//     bool hasGoodPosition = false;

    for(ushort ss = 0; ss < m_inst.stacks(); ++ss) {
        if(m_empty[ss]) return true; // we can always put the container in an empty stack. // TODO Take into account the removal of containers above this one in terms of blocking the empty stack
        ushort tt = 0;
        bool overstowing = false;
        for(; tt < m_inst.tiers() && tt < m_stackTop[ss] && atCell(tt, ss) >= cval && !overstowing; ++tt) {
            const cell& blocking = atCell(tt, ss);
            overstowing = tt > 0 && blocking > atCell(tt - 1, ss);
        }
        if(tt < m_inst.tiers() && !overstowing) {
            const cell& inNewPos = atCell(tt, ss);
            const cell& aboveNewPos = atCell(tt + 1, ss); 
            if(tt == m_inst.tiers() - 1 || inNewPos >= aboveNewPos) {
                // (tt, ss) is the highest well-placed container in the
                // stack, meaning we can place the container to be moved on
                // top of it assuming it is free or the overstowed
                // containers can be moved.
                if(tt < m_inst.tiers() - 1 && aboveNewPos == 0) return true; // the new position has nothing stored above it, so it can be used.
                bool canMoveAbove = true;
                for(ushort tt2 = tt; tt2 < m_inst.tiers() && canMoveAbove; ++tt2) {
                    canMoveAbove = hasGoodPosition(tt2, ss, level + 1);
                } 
                // TODO Placing a container back into its own stack necessitates an extra move!
                // DEBUG
                if(canMoveAbove) std::cout << "\t\tPlace " << cval << " above ss: " << ss << "; tt: " << tt << std::endl;
                // /DEBUG
                if(canMoveAbove) return true;
            }
        } // else: this stack is full and moving any of these containers would not make sense
    }

    return false;
}


