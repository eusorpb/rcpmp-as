#ifndef PM_SEARCH
#define PM_SEARCH

#include <cassert>
#include <vector>

#include "typedefs.h"
#include "Instance.h"


struct Move {
    Move(short ff, short tt) : from(ff), to(tt) {}
    short from;
    short to;
};

class IdaSearch {
public:
    enum LowerBound {
        LB_BF, // Bortfeldt & Forster LB
        LB_DIRECT // Direct moves LB (easy to ensure corectness, but not a strong LB)
    };

public:
    IdaSearch(const Instance& inst, bool quiet = false);
    ~IdaSearch();

    // Returns the number of moves necessary to sort the stacks
    short idaStarSearch();

    // Computes the heuristic cost of the solution stored at treeDepth in m_cells.
    ushort heuristicCost();
    ushort heuristicCostBf();
    ushort heuristicCostCustom(bool onlyDirect = false);
//     // Precondition: it is currently assumed that heuristicCostBf has set lbHighestWplaced TODO move this into a helper function
//     ushort heuristicCostInd();

    inline size_t cellOffset(const short& tier, const short& stack) const {
        return tier * m_inst.stacks() + stack;
    }

    inline cell& atCell(const short& tier, const short& stack) {
        return m_cells[cellOffset(tier, stack)];
    }

    inline const cell& atCell(const short& tier, const short& stack) const {
        return m_cells[cellOffset(tier, stack)];
    }

    inline cell& atCCell(const short& tier, const short& stack) {
        return m_ccells[cellOffset(tier, stack)];
    }

    inline const cell& atCCell(const short& tier, const short& stack) const {
        return m_ccells[cellOffset(tier, stack)];
    }

    inline long nodesOpened() const { return m_nodesOpened; }
    inline long sbUnrelCount() const { return m_sbUnrelCount; }
    inline long sbUnrelMultiCount() const { return m_sbUnrelMultiCount; }
    inline long sbTransCount() const { return m_sbTransCount; }
    inline long sbTransMLCount() const { return m_sbTransMLCount; }
    inline long sbEmptyCount() const { return m_sbEmptyCount; }
    inline long greedyCompletionsAttempted() const { return m_greedyCompletionsAttempted; }
    inline long greedyCompletionsSuccess() const { return m_greedyCompletionsSuccess; }

    // Determines whether the solution at the specified treeDepth in m_cells is
    // a valid pre-marshalling problem solution (i.e., no overstowage).
    bool isSolution() const;

    // stackBottom indicates whether to print stack labels
    void printCurrentCells(bool stackBottom = false) const;
    // Prints each step of the solution, starting with the solution leading back to the start.
    void printSolutionTrail();

    inline LowerBound lowerBound() const { return m_useLb; }
    inline long bfBetter() const { return m_bfBetter; }
    inline long usBetter() const { return m_customBetter; }
    inline long lbTotal() const { return m_lbTotal; }
    inline long usSkipped() const { return m_customSkipped; }

private:
    // Calls itself for each possible movement of a single container (less
    // broken symmetries) in the current configuration of m_cells.
    // Parameters: moves is the number of moves to reach this level, maxDepth is the maximum depth
    bool recurse(ushort moves, ushort maxDepth);

    // Performs a greedy completion of the current solution by shifting
    // containers according to a minimum priority difference heuristic that
    // only performs "good" moves, i.e., non-overstowing moves. If the
    // heuristic solves the problem, then the number of moves performed using
    // the algorithm is optimal. When an empty stack is available, the
    // overstowing container with the highest priority is moved there.
    //
    // Precondition: the number of overstowing containers is less than or equal
    // to the number of moves left at this IDA depth.
    // 
    // maxMoves: the maximum number of moves to use to try to find a solution.
    // reverseState: Indicates that the solution should be returned the state
    // when the method was called, even if successful.
    //
    // Returns: -1 on failure, otherwise the number of moves needed to solve the problem.
//     int greedyComplete(const ushort& maxMoves, const bool& reverseState = true);

//     inline bool performMove(ushort fromStack, ushort toStack, bool moveCap = false, bool undoMove = false) {
    inline bool performMove(ushort fromStack, ushort toStack) {
        atCell(m_stackTop[toStack] + 1, toStack) = atCell(m_stackTop[fromStack], fromStack);
        atCell(m_stackTop[fromStack], fromStack) = 0;

        atCCell(m_stackTop[toStack] + 1, toStack) = atCCell(m_stackTop[fromStack], fromStack);
        atCCell(m_stackTop[fromStack], fromStack) = 0;

        m_stackTop[fromStack]--;
        m_stackTop[toStack]++;
        m_empty[toStack] = false;
        m_empty[fromStack] = m_stackTop[fromStack] < 0;
        return true;
    }

    // Attempts to place the container at position (tier, stack) in a different stack
    // without causing any container movements that would not have been
    // required anyway (i.e., direct moves are allowed, but Bad-Bad or Good-Bad
    // are not)
    // 
    // banStack can be used to prevent the container from using a particular
    // stack (other than "stack", which is already banned). This is mainly used
    // for recursion of hasGoodPosition. To ban no stacks, set banStack to
    // m_inst.stacks()
    //
    // Returns true if this container can be moved somewhere that does not require extra moves
    // Returns false if moving this container will necessarily increase the lower bound
    bool hasGoodPosition(const ushort& tier, const ushort& stack, ushort level = 0);

    // Returns whether or not it is worthwhile to try our lower bound on the
    // current cells.
    inline bool tryOurLowerBound() const {
        // Right now: only use our LB when the highest priority container is overstowing
        for(ushort ss = 0; ss < m_inst.stacks(); ++ss) {
            if(atCell(0, ss) == m_inst.latestPriority()) return false;
        }
        return true;
    }

private:
    const Instance& m_inst;

    // Represents a two dimensional array of the following form:
    // [tier][stack]
    // The bottom of each stack is located at tier 0
    std::vector<cell> m_cells;
    std::vector<cell> m_ccells;
    
    // Provides the tier of the top container in each stack
    std::vector<short> m_stackTop;
    // Indicates whether a stack is empty or not
    std::vector<bool> m_empty;
//     int m_numEmpty;
    
    std::vector<Move> m_moves;

    std::vector<int> m_lbDemand;
    std::vector<int> m_lbSupply;
    std::vector<int> m_lbHighestWplaced;
    std::vector<bool> m_lbStackHasOverstowage;
    short m_directMoves;

    std::vector<bool> m_tmlAllowed; // used for multi-level transitive symmetry breaking; size: stacks * IDA depth

    short m_tiersM1; // tiers minus 1

    long m_nodesOpened;
    long m_sbUnrelCount;
    long m_sbUnrelMultiCount;
    long m_sbTransCount;
    long m_sbTransMLCount;
    long m_sbEmptyCount;
    long m_greedyCompletionsAttempted;
    long m_greedyCompletionsSuccess;

    long m_bfBetter;
    long m_customBetter;
    long m_customSkipped;
    long m_lbTotal;

    // Cost to clear a stack down to its highest well placed container
    // Note: currently using weak LB
    // TODO Try strong version
    std::vector<ushort> m_clearCost;

    LowerBound m_useLb;

//     std::vector<short> m_containerAtPos;
//     std::vector<short> m_numMoves;

//     bool m_canRunGreedy; // set in heuristicCost if the conditions for using the greedy algorithm are met
//     int m_stackOverstowage; // set in heuristicCost so we can see if it is worth running greedy

};

#endif
