# README #

This project contains code to generate instances for the robust container pre-marshalling problem (RCPMP). It is licensed using the BSD-new license. The code and data in this repository is presented in an ICCL 2016 proceedings paper. This paper will be added to this repository shortly.

## Building & Running ##

### Dependencies ###

This source code requires the gflags library and a MiniZinc-compatible CP
solver available on the command line. By default, the solver mzn-g12lazy is
used, but this can be changed using the --minizincPath argument.

### Building the code ###

If the dependencies are met, try building the code:

    cd code
    make

The output is the executable robustpm.

### Running the code ###

There are a number of command line options for running robust pm. To see them, simply type:

    ./robustpm --help

The basic usage is:

    ./robustpm [flags] <input file>

## Instance generation ##

The python code gen.py can be used to generate RCPMP instances. Please run the
tool using python3 on the command line for options. The instances we used in
our ICCL 2016 paper are available in gen\_lt. An equivalent dataset to the one
provided in gen\_lt can be created using the gen\_lt.sh script.

Note that the instances generated are not guaranteed to be feasible. While this
would be possible, for example by generating a sequence of moves to perturb a
clean bay, we do not do this to avoid biasing the dataset towards any
particular class of instances.

## Citing this work ##

If you wish to cite this work, please use the following reference:

Tierney, K. and Voß, S.: Solving the Robust Container Pre-Marshalling Problem. Accepted to the 7th International Conference on Computational Logistics, 2016.

    @incollection{rcpmpiccl16,
        year={2016},
        booktitle={Computational Logistics},
        series={Lecture Notes in Computer Science},
        title={Solving the Robust Container Pre-Marshalling Problem},
        publisher={Springer Berlin},
        pages={131--145},
        author={Tierney, K. and Vo{\ss}, S.},
        editor={Paias, A. and Ruthmair, M. and Vo{\ss}, S.}
    }

## References ##

Tierney, K., Pacino, D., Voss, S.: Solving the pre-marshalling problem to optimality with A* and IDA*. Flexible Services and Manufacturing (2016). (In Press)